import { Context } from 'koa';
import PeopleService from '../services/PeopleService';

interface IPeopleController {
    create(ctx: Context, next: Function): Promise<void>;
    read(ctx: Context, next: Function): Promise<void>;
    // readAll(ctx: Context, next: Function): Promise<void>;
    update(ctx: Context, next: Function): Promise<void>;
    delete(ctx: Context, next: Function): Promise<void>;
}

class PeopleController implements IPeopleController {
    async create(ctx: Context, next: Function): Promise<void> {
        console.info('Creating new member on board');
        const member = ctx.request.body;
        try {
            const insertedMember = await PeopleService.insertMember(member);
            ctx.ok({
                member: insertedMember,
            });
        } catch (err) {
            ctx.internalServerError();
        }
        next();
    }
    async read(ctx: Context, next: Function): Promise<void> {
        console.info('Getting member');
        const memberId = ctx.params.memberId;
        try {
            const member = await PeopleService.getMember(memberId);
            ctx.ok({
                member: member,
            });
        } catch (err) {
            ctx.internalServerError();
        }
        next();
    }
    async readAll(ctx: Context, next: Function): Promise<void> {
        console.info('Getting all members aboard');
        try {
            const allMembers = await PeopleService.getAllMembers();
            ctx.ok({
                members: allMembers,
            });
        } catch (err) {
            ctx.internalServerError();
        }
        next();
    }
    async update(ctx: Context, next: Function): Promise<void> {
        console.info('Updating member info');
        const memberId = ctx.params.memberId;
        const member = ctx.request.body;
        try {
            const updatedMember = await PeopleService.updateMember(memberId, member);
            ctx.ok({
                member: updatedMember,
            });
        } catch (err) {
            ctx.internalServerError();
        }
        next();
    }
    async delete(ctx: Context, next: Function): Promise<void> {
        console.info('Removing member info');
        const memberId = ctx.params.memberId;
        try {
            await PeopleService.deleteMember(memberId);
            ctx.ok();
        } catch (err) {
            ctx.internalServerError();
        }
        next();
    }
}

export default new PeopleController();
