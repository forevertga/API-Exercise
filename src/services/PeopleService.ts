import { PeopleModel, IPeople } from '../models/People';

interface IPeopleService {
    insertMember(member: IPeople): Promise<IPeople>;
    getMember(memberId: string): Promise<IPeople | null>;
    getAllMembers(): Promise<IPeople[] | null>;
    updateMember(memberId: string, member: IPeople): Promise<IPeople | null>;
    deleteMember(memberId: string): Promise<any>;
}

class PeopleService implements IPeopleService {
    async insertMember(member: IPeople): Promise<IPeople> {
        return PeopleModel.create(member);
    }
    async getMember(memberId: string): Promise<IPeople | null> {
        return PeopleModel.findById(memberId);
    }
    async getAllMembers(): Promise<IPeople[] | null> {
        return PeopleModel.find()
    }
    async updateMember(memberId: string, member: IPeople): Promise<IPeople | null> {
        const options = {
            upsert: true,
            new: true

        };
        return PeopleModel.findByIdAndUpdate(memberId, member, options);
    }
    async deleteMember(memberId: string): Promise<any> {
        return PeopleModel.findByIdAndDelete(memberId);
    }
}

export default new PeopleService();
