import PeopleController from '../controllers/PeopleController';
import Router from 'koa-router';

const router = new Router({ prefix: '/people' })
    .get('/:memberId', async (ctx, next) => {
        await PeopleController.read(ctx, next);
    })
    .get('/', async (ctx, next) => {
        await PeopleController.readAll(ctx, next);
    })
    .put('/:memberId', async (ctx, next) => {
        await PeopleController.update(ctx, next);
    })
    .del('/:memberId', async (ctx, next) => {
        await PeopleController.delete(ctx, next);
    })
    .post('/', async (ctx, next) => {
        await PeopleController.create(ctx, next);
    });

export default () => router.routes();
