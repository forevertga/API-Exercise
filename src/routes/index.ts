import PeopleRoutes from './PeopleRoutes';
import Router from 'koa-router';
import compose from 'koa-compose';

const router = new Router({ prefix: '/api' }).use(PeopleRoutes());

export default () => compose([router.routes(), router.allowedMethods()]);
