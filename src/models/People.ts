import { Schema, Document, model, Types } from 'mongoose';

export interface IPeople extends Document {
    _id: Types.ObjectId;
    survived: boolean;
    pclass: number;
    name: string;
    sex: string;
    age: number;
    siblingsOrSpousesAboard: number;
    parentsOrChildrenAboard: number;
    fare: number;
}

const PeopleSchema: Schema = new Schema(
    {
        survived: {
            type: Boolean,
            required: true,
        },
        pclass: {
            type: Number,
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        sex: {
            type: String,
            required: true,
        },
        age: {
            type: Number,
            required: true,
        },
        siblingsOrSpousesAboard: {
            type: Number,
            required: true,
        },
        parentsOrChildrenAboard: {
            type: Number,
            required: true,
        },
        fare: {
            type: Number,
            required: true,
        },

    },
    { timestamps: true, versionKey: false }
);

export const PeopleModel = model<IPeople>('titanics', PeopleSchema);
