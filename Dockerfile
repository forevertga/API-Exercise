# Compile TS
FROM node:12.18-buster-slim as builder
LABEL stage=builder
RUN apt-get -y update
WORKDIR /app/csapi
COPY package*.json ./
COPY ./src ./src
COPY tsconfig.json .
RUN npm i -s && npm run dist

FROM node:12.18-alpine as release
RUN mkdir -p /home/node/csapi/node_modules && chown -R node:node /home/node/csapi
WORKDIR /home/node/csapi
COPY package*.json ./
COPY --from=builder /app/csapi/dist ./dist
USER node
RUN npm i --production --no-optional -s
# Waiter setup for cases when using docker-compose
ENV WAIT_VERSION 2.7.3
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

CMD ["./start.sh"]